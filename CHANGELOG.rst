Changelog
=========

Development
-----------

- Add manual resource management methods to producer and consumer classes
  (`MR29 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/29>`)

1.0.0
-----

- Use ska-sdp-datamodels 1.0.0
  (`MR27 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/27>`__)
- Various dependency updates - see linked MR
  (`MR27 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/27>`__)
- Convert markdown (.md) files to restructuredtext (.rst) - includes README.md, CHANGELOG.md
  (`MR27 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/27>`__)
- Updates to the Signal Metric data structures
  (`MR26 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/26>`__)

  - Convert all Signal Metrics types to pydantic dataclasses
  - Add statistics objects used by QA Displays that are sent by the
    Visibility Receive process

- Add ``npy`` data format support
  (`MR25 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/25>`__)
- Remove the SPECTROGRAM type from MetricDataTypes
  (`MR24 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/24>`__)

0.4.0
-----

- Refactor of the ``DataQueue`` class
  (`MR23 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/23>`__)

  - [Deprecate] ``DataQueue.load_from_queue``, this is in favour of
    switching to streaming.
  - [Migration/Breaking] ``DataQueueMultiTopic``\ ’s features have been
    put into ``DataQueue``, and removed.
  - [New] Now supports streaming directly with an async iterator.
  - [New] Now supports updating the topics and partitions for the
    consumer.
  - [New] Topics can be created manually.
  - [New] Partition counts can be created for each partition manually.

- Allow setting a partition when sending and receiving messages
  (`MR22 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/22>`__)

.. _section-1:

0.3.0
-----

- Allow unsubscribing from topics in DataQueueMultiTopic
  (`MR19 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/19>`__)
- Allow consumer to start without group_id. NOTE: this should only be
  used with DataQueueMultiTopic
  (`MR19 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/19>`__)
- xradio made an optional dependency
  (`MR19 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/19>`__)
- DataQueue awrite and aread takes topic as input
  (`MR17 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/17>`__)
- DataQueue closes producer and consumer on exit
  (`MR16 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/16>`__)
- ``decode`` and ``encode`` are their own function, not DataQueue
  methods
  (`MR16 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/16>`__)

.. _section-2:

0.2.0
-----

- Add option to encode/decode xarray datasets when sending to Kafka
  (`MR13 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/13>`__)
- Changing metric names for Signal Display Metrics to avoid underscores
  (`MR14 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/14>`__)

.. _section-3:

0.1.2
-----

- Addition of DataAndComponentPayload structure
  (`MR12 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/12>`__)

.. _section-4:

0.1.1
-----

- Update Signal Metrics Schemas
  (`MR11 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/11>`__)

.. _section-5:

0.1.0
-----

- Update package dependency versions
  (`MR10 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/10>`__)

.. _section-6:

0.0.1
-----

- Implement functionality for loading data from the queue
  (`MR6 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/6>`__)
- Add schema validation checks for existing three schemas
  (`MR7 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/7>`__)
- Add a class for pushing data to a Kafka queue with an empty
  placeholder for loading from queue
  (`MR3 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/3>`__)
- Add schema for structured numpy array version of pointing offset data
  (`MR4 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/4>`__)
- Add xradio-compatible scheme for the pointing table class
  (`MR2 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/2>`__)
- Add schema for signal display metric data
  (`MR5 <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues/-/merge_requests/5>`__)
