include .make/base.mk
include .make/python.mk
include .make/oci.mk

PROJECT_NAME = ska-sdp-dataqueues
CHANGELOG_FILE = CHANGELOG.rst

# F821: undefined name -> when the numpy dtypes are defined in the file
# E402: module level import not at top of file
# W503: line break before binary operator -> incompatible with black
PYTHON_SWITCHES_FOR_FLAKE8 = --per-file-ignores="src/ska_sdp_dataqueues/schemas/numpy_structured_pointing.py:F821" --ignore=E402,W503

# C0415: import-outside-toplevel
# C0413: wrong-import-position
PYTHON_SWITCHES_FOR_PYLINT = --max-args=7 --max-positional-arguments=7 --disable=C0415,C0413