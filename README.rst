SKA SDP Data Queue Library
==========================

The data queue library supports loading data on and off Kafka queues. In
addition, it provides schemas for validating the data before sending to
a queue.

The
`Documentation <https://developer.skao.int/projects/ska-sdp-dataqueues/en/latest/>`__
includes usage examples, API, and installation directions.

The CI/CD occurs on
`Gitlab <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues.git>`__.

Standard CI machinery
---------------------

This repository is set up to use the
`Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile>`__
and `CI jobs <https://gitlab.com/ska-telescope/templates-repository>`__
maintained by the System Team. For any questions, please look at the
documentation in those repositories or ask for support on Slack in the
#team-system-support channel.

To keep the Makefiles up to date in this repository, follow the
instructions at:
https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile#keeping-up-to-date

Contributing to this repository
-------------------------------

`Black <https://github.com/psf/black>`__,
`isort <https://pycqa.github.io/isort/>`__, and various linting tools
are used to keep the Python code in good shape. Please check that your
code follows the formatting rules before committing it to the
repository. You can apply Black and isort to the code with:

.. code:: bash

   make python-format

and you can run the linting checks locally using:

.. code:: bash

   make python-lint

The linting job in the CI pipeline does the same checks, and it will
fail if the code does not pass all of them.

Creating a new release
----------------------

When you are ready to make a new release (maintainers only):

- Check out the main branch
- Update the version number in ``.release``, ``pyproject.toml``, and
  ``docs/src/conf.py`` with

  - ``make bump-patch-release``,
  - ``make bump-minor-release``, or
  - ``make bump-major-release``

- Manually replace ``main`` with the new version number in
  ``CHANGELOG.md``
- Create the git tag with ``make git-create-tag`` When it asks for the
  JIRA ticket, use the ORCA ticket that you are working on
- Push the changes with ``make git-push-tag``
