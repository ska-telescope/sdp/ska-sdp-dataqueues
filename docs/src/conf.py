# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "SDP Data Queue Library"
copyright = "2024-2025 SKA SDP Developers"
author = "SKA SDP Developers"
version = "1.0.0"
release = "1.0.0"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx_new_tab_link",
    "enum_tools.autoenum",
]

exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"

# -- Extension configuration -------------------------------------------------

autodoc_mock_imports = ["xradio"]

intersphinx_mapping = {
    "ska-sdp-qa-data-api": (
        "https://developer.skao.int/projects/ska-sdp-qa-data-api/en/latest/",
        None,
    ),
}

autodoc_default_options = {"member-order": "bysource"}
