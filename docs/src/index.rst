.. _documentation_master:


SKA SDP Data Queue Library
##########################

This is a `repository <https://gitlab.com/ska-telescope/sdp/ska-sdp-dataqueues.git>`_
for the Data Queue Library used in the SKA SDP.

The data queue library supports loading data onto and off Kafka queues. In addition,
it provides schemas for validating the data before sending to a queue.


.. toctree::
  :maxdepth: 1
  :caption: Contents:

  data_queue
  data_queue_api
  schemas
  schemas_api