.. _schemas:

Schemas for Validation
======================

Currently, the package supports schemas from the below categories

- Pointing information: Outputs from the pointing offset calibration pipeline
- Signal Display: Metrics used by signal display API

For a detailed API of the various schemas, refer to :ref:`schema_api`.


Pointing Information
--------------------

Two schemas are supported for pointing information, numpy structured arrays and the PointingTable dataclass.
The PointingTable is constructed following the format of schemas defined in `xradio`_.


Signal Display Metrics
----------------------

These datamodels are used to transmit the metrics from the Visibility Receive
Process to the Signal Display API for use with creating the graphs required on
the UI.

Refer to the :external+ska-sdp-qa-data-api:doc:`Signal Display API documentation <display/overview>` for more information


.. _xradio: https://github.com/casangi/xradio.git