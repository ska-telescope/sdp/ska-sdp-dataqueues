.. _schema_api:

Schema APIs
===========

Pointing Table
--------------

.. automodule:: ska_sdp_dataqueues.schemas.pointing_table
    :members:

Pointing data - Structured numpy array
--------------------------------------

.. automodule:: ska_sdp_dataqueues.schemas.numpy_structured_pointing
    :members:

Signal Display Metrics
----------------------

.. automodule:: ska_sdp_dataqueues.schemas.signal_display_metrics
    :members:
