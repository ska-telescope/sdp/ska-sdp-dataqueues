"""
A collection of classes that provides access to Kafka.
"""

import json
import logging
from collections.abc import AsyncIterator
from dataclasses import is_dataclass
from enum import Enum
from io import BytesIO
from time import time

import msgpack
import msgpack_numpy
import numpy as np
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from aiokafka.admin.client import AIOKafkaAdminClient
from aiokafka.admin.new_partitions import NewPartitions
from aiokafka.admin.new_topic import NewTopic
from aiokafka.structs import RecordMetadata, TopicPartition
from ska_sdp_datamodels.utilities import decode as datamodels_decode
from ska_sdp_datamodels.utilities import encode as datamodels_encode

from ska_sdp_dataqueues.schemas.check import (
    AnyDataclass,
    DataType,
    check_other_schema,
)

logger = logging.getLogger(__name__)


# pylint: disable=too-many-instance-attributes


class Encoding(str, Enum):
    """The encode/decode method to use"""

    UTF8 = "utf-8"
    ASCII = "ascii"
    MSGPACK_NUMPY = "msgpack_numpy"
    NPY = "npy"
    XARRAY = "xarray"
    JSON = "json"
    BYTES = "bytes"


class DataQueueProducer:
    """
    A Producer object makes a connection to a Kafka server and allows messages
    to be pushed to specified topics, and optionally specific partitions.

    There is an option to set the maximum message size, which
    defaults to 1 MiB (2**20 bytes) as recommended by Kafka. Sending
    larger messages than this will linearly increase latency.

    :param server: The address(es) for the Kafka Broker to query for
        metadata and set up the connection.
    :param topic: The default Kafka topic to write messages to
    :param message_max_bytes: maximum message size for push
    :param encoding: The default encoding to use (or None to specify for each
        message)
    """

    def __init__(
        self,
        server: str | list[str],
        message_max_bytes: int = 2**20,
        topic: str | None = None,
        encoding: Encoding = None,
    ) -> None:

        self.server = server
        self.message_max_bytes = message_max_bytes

        # Set up the Producer
        self._producer = None
        self._topic = topic
        self._encoding = encoding

    async def astart(self):
        """Start this producer"""
        await self._producer_start()

    async def astop(self):
        """Stop this producer"""
        await self._producer_stop()

    async def __aenter__(self):
        """Use class as contextmanager"""
        await self.astart()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        """Stop consumer and producer upon exit"""
        await self.astop()

    async def _producer_start(self):
        self._producer = AIOKafkaProducer(
            bootstrap_servers=self.server,
            max_request_size=self.message_max_bytes,
        )

        await self._producer.start()

    async def _producer_stop(self):
        await self._producer.flush()
        await self._producer.stop()

    async def send(
        self,
        data: DataType,
        encoding: Encoding | None = None,
        schema: AnyDataclass | None = None,
        validate: bool = False,
        topic: str | None = None,
        partition: int | None = None,
    ) -> RecordMetadata:
        """
        Sends data to the Kafka topic

        :param data: Data to send to Kafka topic
        :param encoding: Encoding type
        :param schema: Schema to validate data
            (either xradio DatasetSchema or a dataclass)
        :param validate: Validate using schema?
        :param topic: The topic to send data to.
            Optional. If not provided the default topic (from the
            constructor will be used).
        :param partition: The explicit partition to send data to.
            Optional. If not provided partitions will be set automatically.
        :return: AIOKafka RecordMetadata
        """
        if topic is None:
            topic = self._topic

        if topic is None:
            raise ValueError(
                "`topic` must either be set in the constructor or when calling"
                " this method"
            )

        if encoding is None:
            encoding = self._encoding

        if encoding is None:
            raise ValueError(
                "`encoding` must either be set in the constructor or when "
                "calling this method"
            )

        logger.debug(
            "Writing data to topic '%s' with encoding '%s' using partition %s",
            topic,
            encoding,
            partition,
        )

        # Validate schema
        if validate:
            if not schema:
                logger.error("Schema is required for validation. Skipping.")
            else:
                _validate_data(data, schema)

        # Check the data here and encode it for byte representation.
        # Note: LMC Queue Connector already has code for serialization
        # which could be used here. For now just add something simple.
        data_to_send = _encode(data, encoding)

        record_metadata = await self._producer.send_and_wait(
            topic, data_to_send, partition=partition
        )

        logger.debug("%s sent to Kafka topic, %s", data, record_metadata.topic)

        return record_metadata


class DataQueueConsumer:
    """
    A Consumer object makes a connection to a Kafka server and allows messages
    to be streamed from specified topics, and optionally specific partitions.

    :param server: The address(es) for the Kafka Broker to query for
        metadata and set up the connection.
    :param topics: The Kafka topics to stream messages from.
    :param encoding: The encoding to use, use ``None`` to get back bytes.
    :param set_group_id: Whether to set a group ID or not (based on server and
        topics)
    :param auto_offset_reset: From where the consumer should start
        (``earliest`` | ``latest``)
    :param set_consumer_offsets: Whether to set the consumer offsets.
    """

    def __init__(
        self,
        server: str | list[str],
        topics: list[str] | None = None,
        encoding: Encoding = Encoding.BYTES,
        set_group_id: bool = True,
        auto_offset_reset="earliest",
        set_consumer_offsets: bool = True,
    ) -> None:

        self.server = server

        # Set up the Producer
        self._consumer = None
        self._topics = [] if topics is None else topics

        self.end_message_ids = {}
        self._encoding = encoding
        if set_group_id and len(self._topics) > 0:
            self._group_id = (
                f"Consumer/{self.server}/{'-'.join(self._topics)}/0"
            )
        else:
            self._group_id = None
        self._auto_offset_reset = auto_offset_reset
        self._set_consumer_offsets = set_consumer_offsets

    async def astart(self):
        """Start this consumer and adjust its offsets"""
        self._consumer = AIOKafkaConsumer(
            *self._topics,
            bootstrap_servers=self.server,
            group_id=self._group_id,
            auto_offset_reset=self._auto_offset_reset,
        )

        await self._consumer.start()
        if self._set_consumer_offsets:
            for topic in self._topics:
                await self._set_consumer_offset(topic)

    async def astop(self):
        """Stop this consumer"""
        await self._consumer.stop()

    async def __aenter__(self):
        """Use class as contextmanager"""
        await self.astart()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        """Stop consumer and producer upon exit"""
        await self.astop()

    async def __aiter__(self) -> AsyncIterator[tuple[str, DataType]]:
        """Stream messages from an open consumer."""
        async for message in self._consumer:
            start = time()
            try:
                yield (
                    message.topic,
                    _decode(message.value, self._encoding),
                )
            finally:
                end = time()
                logger.info(
                    "Data Received: %s | %.1f KB | took %d ms",
                    message.topic,
                    len(message.value) / 1024,
                    int((end - start) * 1000),
                )

    async def _set_consumer_offset(self, topic):
        """
        Set consumer offset, i.e. what ID to read messages from
        a topic, if the consumer goes down (where to start again).

        NOTE: only use if you have started the consumer with
        `set_group_id=True`
        """
        # Get the partition(s) used for this topic
        partitions = self._consumer.partitions_for_topic(topic)
        if not partitions:
            partitions = []

        topic_partitions = []
        for partition in partitions:
            topic_partitions.append(TopicPartition(topic, partition))
        logger.debug("Partitions found = %s", topic_partitions)

        # The next available message ID offset on each partition
        end_message_ids = await self._consumer.end_offsets(topic_partitions)

        # Subtract one from each ID to get the last used message ID
        for topic_partition in topic_partitions:
            self.end_message_ids[topic_partition] = end_message_ids[
                topic_partition
            ]
            logger.debug(
                "Last used message ID offset on partition %s = %s",
                topic_partition,
                self.end_message_ids[topic_partition] - 1,
            )
            last_committed = await self._consumer.committed(topic_partition)
            logger.debug(
                "Messages available to read from ID offset %s", last_committed
            )

    async def update_topics(self, topics: list[str]):
        """Update the list of topics that can be consumed.

        Note this will replace all topics subscribed to."""
        if not topics:
            self._consumer.unsubscribe()
        else:
            self._consumer.subscribe(topics=topics)

    async def assign_topics(self, topic_partitions: list[tuple[str, int]]):
        """Update the list of topics (and partitions) that can be consumed.

        Note this will replace all topics subscribed to.

        Example call:
        ``admin.assign_topics([("topic-name", 0), ("topic-name", 3)])``

        Would then consume messages from ``topic-name`` using only partitions
        ``0`` and ``3``"""

        self._consumer.assign(
            [TopicPartition(t[0], t[1]) for t in topic_partitions]
        )


class DataQueueAdmin:
    """
    An Admin object allows for priveledge access to the cluster, this can be
    used to create new topics/partitions, and to list the current
    topics/partitions.

    :param server: The address(es) for the Kafka Broker to query for
        metadata and set up the connection.
    """

    def __init__(self, server: str | list[str]) -> None:
        self.server = server
        self._admin = None

    async def __aenter__(self):
        """Use class as contextmanager"""
        self._admin = AIOKafkaAdminClient(bootstrap_servers=self.server)
        await self._admin.start()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        """Stop consumer and producer upon exit"""
        await self._admin.close()

    async def list_topics(self) -> list[dict]:
        """List the currently created topics"""
        return await self._admin.describe_topics()

    async def create_topics(self, topics: list[str] | list[tuple[str, int]]):
        """Create a list of topics, or create topics with partition counts.

        ``topics`` should either be:

        - ``topics=["topic1", "topic2"]``
        - ``topics=[("topic1", 1), ("topic2", 10)]``

        The two types can be mixed, and in most cases the first should be used.
        """
        if len(topics) == 0:
            logger.warning("Topic list is empty")
            return

        current_topics_raw = await self.list_topics()

        current_topics = {
            topic["topic"]: len(topic["partitions"])
            for topic in current_topics_raw
        }

        topic_creation = []
        topic_updates = {}

        for topic in topics:
            if isinstance(topic, str):
                if topic not in current_topics:
                    topic_creation.append(
                        NewTopic(
                            name=topic, num_partitions=1, replication_factor=1
                        )
                    )
            elif isinstance(topic, tuple):
                if topic[0] not in current_topics:
                    topic_creation.append(
                        NewTopic(
                            name=topic[0],
                            num_partitions=topic[1],
                            replication_factor=1,
                        )
                    )
                elif topic[1] > current_topics[topic[0]]:
                    topic_updates[topic[0]] = NewPartitions(topic[1])

            else:
                raise ValueError("Only str or tuple[str, int] is supported")

        if len(topic_creation) > 0:
            logger.info(
                "Creating topics: %s",
                [
                    f"{topic.name}|{topic.num_partitions}"
                    for topic in topic_creation
                ],
            )
            await self._admin.create_topics(topic_creation)
        if len(topic_updates) > 0:
            logger.info(
                "Updating partitions: %s",
                [
                    f"{topic}|{partition.total_count}"
                    for topic, partition in topic_updates.items()
                ],
            )
            await self._admin.create_partitions(topic_updates)


def _encode(value: DataType, encoding: Encoding) -> bytes:
    """
    Serialization function to encode data of different
    types to bytes.

    :param value: input data
    :param encoding: encoding for output bytes representation
    """
    match encoding:
        case Encoding.MSGPACK_NUMPY:
            return msgpack.packb(value, default=msgpack_numpy.encode)
        case Encoding.NPY:
            buffer = BytesIO()
            np.save(buffer, value)
            return buffer.getbuffer().tobytes()
        case Encoding.UTF8 | Encoding.ASCII:
            return str(value).encode(encoding)
        case Encoding.JSON:
            return json.dumps(value).encode("utf-8")
        case Encoding.XARRAY:
            return datamodels_encode(value)
        case _:
            raise ValueError(f"Cannot deal with {encoding} encoding!")


def _decode(message_bytes: bytes, encoding: Encoding) -> DataType:
    """
    Decode message using specified encoding

    :param message_bytes: input data to be decoded
    :param encoding: encoding to use
    """

    match encoding:
        case Encoding.MSGPACK_NUMPY:
            return msgpack.unpackb(
                message_bytes, object_hook=msgpack_numpy.decode
            )
        case Encoding.NPY:
            return np.load(BytesIO(message_bytes))
        case Encoding.UTF8 | Encoding.ASCII:
            return message_bytes.decode()
        case Encoding.JSON:
            return json.loads(message_bytes)
        case Encoding.XARRAY:
            return datamodels_decode(message_bytes)
        case Encoding.BYTES:
            return message_bytes
        case _:
            raise ValueError(f"Cannot deal with {encoding} encoding!")


def _validate_data(data: DataType, schema: AnyDataclass):
    """
    Validate input data to kafka queue.
    Can validate against:
        - dataclass model
        - xradio DatasetSchema

    :param data: input data to be validated
    :param schema: schema to validate against
    """
    if not is_dataclass(schema):
        raise TypeError("Schema must be xradio DatasetSchema or a dataclass")

    try:
        from xradio.schema import check
        from xradio.schema.bases import AsDataset
        from xradio.schema.dataclass import xarray_dataclass_to_dataset_schema
        from xradio.schema.metamodel import DatasetSchema

    except ImportError:
        logger.warning(
            "xradio is not installed. xarray validation will not work. "
            "Trying to validate against dataclass schema."
        )

        check_other_schema(data, schema)
        logger.info(
            "Schema validation performed using dataclass fields and formats"
        )

    else:
        # If schema is an xarray-dataclass derived from xradio
        # AsDataset, convert it to xradio DatasetSchema (so
        # that xradio checker can be used)
        if (not isinstance(schema, DatasetSchema)) and (
            AsDataset in schema.__bases__
        ):
            schema = xarray_dataclass_to_dataset_schema(schema)
            logger.info("Converted input schema to xradio DatasetSchema")

        if isinstance(schema, DatasetSchema):
            issues = check.check_dataset(data, schema)
            logger.info("Schema validation performed using xradio checker")
            if issues:
                raise TypeError(issues)
        else:
            check_other_schema(data, schema)
            logger.info(
                "Schema validation performed using "
                "dataclass fields and formats"
            )
