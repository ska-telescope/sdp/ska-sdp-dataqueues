"""SKA SDP Data Queue Schemas."""

import importlib

from .numpy_structured_pointing import PointingNumpyArray
from .signal_display_metrics import MetricPayload

__all__ = ["PointingNumpyArray", "MetricPayload"]

if importlib.util.find_spec("xradio"):
    from .pointing_table import PointingTable

    __all__ += ["PointingTable"]
