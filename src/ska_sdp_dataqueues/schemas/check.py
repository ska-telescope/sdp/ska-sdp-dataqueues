"""
Functions for schema checks
"""

from dataclasses import Field, is_dataclass
from types import UnionType
from typing import Any, ClassVar, Protocol, Union, get_args, get_origin

import numpy

# Python compatible datatypes for exchange streams.
DataType = (
    tuple[str, bytes]
    | numpy.ndarray
    | dict[str, object]
    | int
    | float
    | bool
    | str
)


class AnyDataclass(Protocol):  # pylint: disable=too-few-public-methods
    """Protocol to denote any dataclass (for type annotations)"""

    __dataclass_fields__: ClassVar[dict[str, Field[Any]]]


def check_other_schema(data: DataType, other_schema: AnyDataclass) -> None:
    """
    Check input data against a non-xarray schema

    :param data: Data to validate
    :param other_schema: Schema class (must be dataclass)
    """

    # schema must be a dataclass with annotations
    if not is_dataclass(other_schema):
        raise TypeError(
            "Schema must be a class with annotations "
            "defined as a dictionary"
        )

    # Check attributes and types match
    annotations = other_schema.__dict__.get("__annotations__", None)

    # For input data = numpy structured array (e.g. PointingNumpyArray)
    if isinstance(data, numpy.ndarray):
        for key, value in annotations.items():
            if data.dtype[key] != value:
                raise TypeError(f"{key}: " f"{data.dtype[key]} != {value}")

    # For input data = dictionary (e.g. MetricPayload)
    elif isinstance(data, dict):
        for key, value in annotations.items():
            # Dig into an item if it is a dataclass
            # (e.g. metrics SpectralWindow)
            if is_dataclass(value):
                dc_anns = annotations[key].__dict__.get("__annotations__", [])
                for dckey, dcvalue in dc_anns.items():
                    if not isinstance(data[key][dckey], dcvalue):
                        raise TypeError(
                            f"[{key}][{dckey}]: "
                            f"{type(data[key][dckey])} != {dcvalue}"
                        )
            # Dig into an item if it is a list (e.g. metrics 'data')
            elif get_origin(value) == list:
                check_data_list(data[key], value, key)
            else:
                if not isinstance(data[key], value):
                    raise TypeError(f"{key}: " f"{type(data[key])} != {value}")
    else:
        raise TypeError(
            f"{type(data)} cannot currently be validated - "
            "must be numpy array or dictionary"
        )


# pylint: disable-next=too-many-branches,too-many-locals
def check_data_list(
    data: DataType,
    schema_item: UnionType | list | DataType,
    key: str,
) -> None:
    """
    Check data item against schema when the schema item type is
    a List containing instances of a dataclass - for example, the
    data attribute of MetricPayload.
    This function digs into the List to match the
    internal type of the list items.

    :param data: Data item to check
    :param schema_item: Schema item to check against
    :param key: The key item being checked
        (to allow identification in exception message)
    """

    data_matches = False

    # Get the type of data forming the list (could be Union)
    types_raw = get_types(schema_item)
    types = []
    for t in types_raw:
        if isinstance(t, UnionType):
            types.extend(get_args(t))
        else:
            types.append(t)

    # loop over types in Union
    # pylint: disable-next=too-many-nested-blocks
    for typ in types:
        # Dig into an item if it is a dataclass
        if is_dataclass(typ):
            checks = []
            dc_annts = typ.__dict__.get("__annotations__", [])
            # Quick check of keys in case of Union
            for dckey in dc_annts.keys():
                checks.append(dckey in data[0].keys())
            # Full check only if all keys match
            if all(checks):
                for dckey, dcvalue in dc_annts.items():
                    key_matches = False
                    data_item = data[0][dckey]
                    styp = get_types(dcvalue)
                    value_type = get_origin(dcvalue)
                    if value_type is Union or value_type is UnionType:
                        for tt in get_args(dcvalue):
                            if get_origin(tt) is list:
                                if isinstance(data_item, list) and isinstance(
                                    data_item[0], get_args(tt)[0]
                                ):
                                    key_matches = True
                            else:
                                if isinstance(data[0][dckey], tt):
                                    key_matches = True
                    elif value_type is list:
                        data_item = data_item[0]
                        key_matches = isinstance(data[0][dckey][0], styp[0])
                    else:
                        key_matches = isinstance(data[0][dckey], styp[0])

                    if not key_matches:
                        raise TypeError(
                            f"[{key}][{dckey}]: "
                            f"{type(data_item)} not in [{styp}]"
                        )
                data_matches = True

    # Must have a match to the data
    if not data_matches:
        raise TypeError(f"{key}: List item types must be in {types}")


def get_types(value: UnionType | list | DataType) -> list:
    """
    Get a list of types from a Union or internal type for a List
    If input is not a Union or a List, just return the
    input type.

    :param value: Type value to expand
    """

    if get_origin(value) is Union:
        typs = list(get_args(value))
    elif get_origin(value) is list:
        typ = get_args(value)[0]
        if get_origin(typ) is Union:
            typs = list(get_args(typ))
        else:
            typs = [typ]
    else:
        typs = [value]

    return typs
