# pylint: disable=no-member,too-many-instance-attributes
# no-member: pylint doesn't recognize dataclass attributes

"""
Schema for a numpy structured array which specifically stores
pointing data as published by the pointing offset
calibration pipeline.
"""

from dataclasses import astuple, dataclass, field

import numpy


@dataclass(frozen=True)
class PointingNumpyArray:
    """
    Schema for structured numpy array containing pointing
    data produced by the Pointing Offset Calibration Pipeline.

    https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/
    """

    antenna_name: numpy.dtype("U8")
    """Name of the antenna (e.g. SKA001)."""
    last_scan_index: numpy.dtype("f8")
    """ID of the last scan in the series of scans used for 
    this pointing calibration."""  # noqa: W291
    xel_offset: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Cross-elevation offset in degrees."""
    xel_offset_std: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Standard deviation of cross-elevation offset."""
    el_offset: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Elevation offset in degrees."""
    el_offset_std: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Standard deviation of elevation offset."""
    expected_width_h: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Expected beam width in horizontal co-polarisation."""
    expected_width_v: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Expected beam width in vertical co-polarisation."""
    fitted_width_h: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Fitted beam width in horizontal co-polarisation."""
    fitted_width_h_std: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Standard deviation of fitted beam in horizontal co-polarisation."""
    fitted_width_v: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Fitted bem width in vertical co-polarisation."""
    fitted_width_v_std: numpy.dtype("f8") = field(metadata={"unit": "degree"})
    """Standard deviation of fitted beam in vertical co-polarisation."""
    fitted_height: numpy.dtype("f8")
    """Fitted Gaussian height in arbitrary units."""
    fitted_height_std: numpy.dtype("f8")
    """Standard deviation of fitted Gaussian height in arbitrary units."""

    def to_numpy(self):
        """Convert dataclass data to structured numpy array."""
        data = astuple(self)
        dtypes = list(self.__annotations__.items())
        return numpy.array([data], dtype=dtypes)
