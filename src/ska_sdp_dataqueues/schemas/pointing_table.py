# pylint:disable=too-many-instance-attributes
"""
List of schemas that store pointing information
Which are compatible with xradio definitions.

"""
from dataclasses import dataclass, field
from typing import Literal, Optional

import numpy
from astropy.coordinates import EarthLocation, SkyCoord
from ska_sdp_datamodels.science_data_model import ReceptorFrame
from xradio.schema.bases import AsDataArray, AsDataset
from xradio.schema.typing import Attr, Coordof, Data

# Dimensions
Time = Literal["time"]
AntennaId = Literal["antenna"]
Frequency = Literal["frequency"]
Receptor = Literal["receptor"]
Angle = Literal["angle"]


# Coordinates / Axes
@dataclass(frozen=True)
class TimeAxis(AsDataArray):
    """Data model of time axis"""

    data: Data[Time, float]
    """Time data"""
    units: Optional[Attr[str]] = "s"
    """Time unit"""
    # Other attributes are set as Optional
    long_name: Optional[Attr[str]] = "Observation Time"
    """Full name of time axis"""
    type: Optional[Attr[str]] = "time"
    """Type of time"""
    scale: Optional[Attr[str]] = "tai"
    """Astropy time scale"""
    format: Optional[Attr[str]] = "unix"
    """Astropy time format"""


@dataclass(frozen=True)
class AntennaAxis(AsDataArray):
    """
    Antenna id of an antenna.
    """

    data: Data[AntennaId, int]
    """Antenna data"""
    long_name: Optional[Attr[str]] = "Antenna ID"
    """Full antenna name"""
    type: Optional[Attr[str]] = "antenna"
    """Antenna type"""


@dataclass(frozen=True)
class FrequencyAxis(AsDataArray):
    """Frequency axis"""

    data: Data[Frequency, float]
    """Frequency data"""
    units: Optional[Attr[str]] = "Hz"
    """Frequency unit"""
    long_name: Optional[Attr[str]] = "Frequency"
    """Full name of frequency axis"""
    type: Optional[Attr[str]] = "spectral_coord"
    """Frequency type"""
    frame: Optional[Attr[str]] = "icrs"
    """Frequency frame"""


@dataclass(frozen=True)
class ReceptorAxis(AsDataArray):
    """
    Possible correlations that can be formed from polarised receptors.
    """

    data: Data[Receptor, str]
    """Receptor data"""
    long_name: Optional[Attr[str]] = "Receptor Frames"
    """Full name of receptor axis"""
    type: Optional[Attr[str]] = "receptor"
    """Receptor type"""


@dataclass(frozen=True)
class AngleAxis(AsDataArray):
    """
    Angle axis
    """

    data: Data[Angle, float]
    """Angle data"""
    units: Optional[Attr[str]] = "rad"
    """Angle unit"""
    frame: Optional[Attr[str]] = "azel"
    """Angle refernce frame"""
    long_name: Optional[Attr[str]] = "2D Angles"
    """Full name of angle axis"""
    type: Optional[Attr[str]] = "angle"
    """Angle axis type"""


@dataclass
class ConfigDict:
    """
    Telescope configuration
    """

    # Required Coordinates
    antenna: Coordof[AntennaAxis]
    """Antenna Axis"""

    # Attributes
    name: Attr[str] = "MID"
    """Name of configuration e.g. 'LOWR3'"""
    location: Attr[EarthLocation] = None
    """Location of array as an astropy EarthLocation"""
    receptor_frame: Attr[str] = "linear"
    """Receptor frame"""
    frame: Attr[str] = "local"
    """Reference frame of locations"""

    # Data
    names: Data[tuple[AntennaId], str] = None
    """Names of each dish/station"""
    diameter: Data[tuple[AntennaId], float] = None
    """Diameters of dishes/stations (m)"""
    mount: Data[tuple[AntennaId], str] = None
    """Mount types of dishes/stations e.g. 'altaz' | 'xy' | 'equatorial'"""
    vp_type: Data[tuple[AntennaId], str] = None
    """Type of voltage pattern (string)"""
    stations: Data[tuple[AntennaId], str] = None
    """Identifiers of the dishes/stations"""
    xyz: Data[tuple[AntennaId, 3], float] = None
    """Geocentric coordinates of dishes/stations"""
    offset: Data[tuple[AntennaId, 3], float] = None
    """Axis offset (m)"""


@dataclass
class PointingTable(AsDataset):
    """
    Data class for pointing information
    """

    # Required Coordinates
    time: Coordof[TimeAxis]
    """Time Axis"""
    antenna: Coordof[AntennaAxis]
    """Antenna Axis"""
    frequency: Coordof[FrequencyAxis]
    """Frequency Axis"""
    receptor: Coordof[ReceptorAxis]
    """Receptor Axis"""
    angle: Coordof[AngleAxis]
    """Angle Axis"""

    # Required data variables / arrays
    # This will eventually also be DataArray classes
    # That contain more information of units etc.
    pointing: Data[tuple[Time, AntennaId, Frequency, Receptor, Angle], float]
    """Pointing data (rad)"""
    weight: Data[tuple[Time, AntennaId, Frequency, Receptor, Angle], float]
    """Weight data (rad)"""
    residual: Data[tuple[Time, Frequency, Receptor, Angle], float]
    """Residual data (rad)"""
    interval: Data[tuple[Time], float]
    """Interval of validity"""
    datetime: Data[tuple[Time], float]
    """Date time of each data point"""

    # Required Attributes
    # Currently centre is default to None
    data_model: Attr[str] = "PointingTable"
    """Data model"""
    receptor_frame: Attr[ReceptorFrame] = field(default_factory=ReceptorFrame)
    """Receptor frame"""
    pointing_frame: Attr[str] = "local"
    """Pointing frame e.g. azel, xel-el"""
    pointingcentre: Attr[SkyCoord] = None
    """Centre of pointing in SkyCoord format"""
    configuration: Attr[ConfigDict] = None
    """Telescope configuration"""

    # Optional data variables/arrays
    nominal: Optional[
        Data[tuple[Time, AntennaId, Frequency, Receptor, Angle], float]
    ] = None
    """Nominal data"""
    expected_width: Optional[
        Data[tuple[Time, AntennaId, Frequency, Receptor, Angle], float]
    ] = None
    """Expected beam width (rad)"""
    fitted_width: Optional[
        Data[tuple[Time, AntennaId, Frequency, Receptor, Angle], float]
    ] = None
    """Fitted beam width (rad)"""
    fitted_width_std: Optional[
        Data[tuple[Time, AntennaId, Frequency, Receptor, Angle], float]
    ] = None
    """Fitted beam width uncertainty (rad)"""
    fitted_height: Optional[
        Data[tuple[Time, AntennaId, Frequency, Receptor], float]
    ] = None
    """Fitted Gaussian height"""
    fitted_height_std: Optional[
        Data[tuple[Time, AntennaId, Frequency, Receptor], float]
    ] = None
    """Fitted height uncertainty"""

    # Optional Attributes
    band_type: Optional[Attr[str]] = None
    """Observing band string"""
    scan_type: Optional[Attr[str]] = None
    """ype of scan string (eg., 5point)"""
    track_duration: Optional[Attr[float]] = None
    """Track duration (sec)"""
    discrete_offset: Optional[Attr[numpy.ndarray]] = None
    """Input discrete pointing relative to target (degrees)"""
    commanded_pointing: Optional[Attr[numpy.ndarray]] = None
    """Commanded pointings (e.g., AzEl, does not have to
    match `pointing_frame`) for each antenna at the
    spatial centre of the pointing observation"""
