"""Signal Display Data Structures.

These Data Structures are primarily used in:

- https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-data-api
- https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-metric-generator

And used to transmit the metric data from the Visibility Receive processor to
the Signal Display API.
"""

import enum
from datetime import datetime

from pydantic import Field, TypeAdapter
from pydantic.dataclasses import dataclass

# pylint: disable=too-many-instance-attributes


@enum.unique
class MetricDataTypes(str, enum.Enum):
    """The current metrics types to be used as a key in MetricPayload"""

    AMPLITUDE: str = "amplitude"
    """Amplitude Metric Type, used for a line graph."""

    PHASE: str = "phase"
    """Phase Metric Type, used for a line graph and creating the
    spectrogram waterfall plots."""

    LAG_PLOT: str = "lagplot"
    """Lag Plot Metric Type, used for waterfall plots."""

    SPECTRUM: str = "spectrum"
    """Spectrum Metric Type, used for a line graph."""

    BAND_AVERAGED_X_CORR: str = "bandaveragedxcorr"
    """Band averaged cross correlation power over time, used for a time series
    plot"""

    UV_COVERAGE: str = "uvcoverage"
    """UV Coverage Metric Type, used for uv coverage and weight distribution
    plots."""


@dataclass
class SpectralWindow:
    """Layout of the Spectral Window"""

    freq_min: float = Field(
        description="The minimum frequency the dataset contains. (in Hz)"
    )

    freq_max: float = Field(
        description="The maximum frequency the dataset contains. (in Hz)"
    )

    count: int = Field(
        description="The number of frequencies the dataset contains."
    )

    channels_id: int = Field(description="The first channel ID.")

    spectral_window_id: int | str = Field(
        description="The id or name of a spectral window"
    )
    start: int = Field(description="The start channel ID")
    stride: int = Field(description="The stride of the channels")


@dataclass
class DataPayload:
    """The payload used for most metric types"""

    baseline: str = Field(
        description="A representation of the baseline this payload contains. "
        "Example `C10_C11`"
    )

    polarisation: str = Field(description="The Polarisation in this payload.")

    data: list[float] | float = Field(
        description="The value(s) of resulting data for this metrics."
    )


@dataclass
class DataAndComponentPayload:
    """
    The payload used for sending the amplitudes + real components,
    or phases + imaginary components of visibilities.
    """

    baseline: str = Field(
        description="A representation of the baseline this payload contains. "
        "Example `C10_C11`"
    )

    polarisation: str = Field(description="The Polarisation in this payload.")

    data: list[float] = Field(
        description="The list of resulting data for this metrics."
    )

    component: list[float] = Field(
        description="The list of either real or imaginary components of "
        "visibilities"
    )


@dataclass
class SpectrumPayload:
    """The payload used for the Spectrum graphs"""

    polarisation: str = Field(description="The Polarisation in this payload.")

    power: list[float] = Field(
        description="The averaged power over all baselines"
    )

    angle: list[float] = Field(
        description="The averaged angle over all baselines"
    )


@dataclass
class UVCoveragePayload:
    """The payload used for the UV coverage and weight distribution plots."""

    baseline: str = Field(
        description="A representation of the baseline this payload contains. "
        "Example `C10_C11`"
    )

    polarisation: str = Field(description="The Polarisation in this payload.")

    weight: float = Field(
        description="The weighting factor for the measurement"
    )

    u: float = Field(description="The u coordinate")

    v: float = Field(description="The v coordinate")

    w: float = Field(description="The w coordinate")


@dataclass
class MetricPayload:
    """The shared metric payload all metrics should use."""

    data_type: MetricDataTypes = Field(description="The current data type")

    processing_block_id: str = Field(
        description="The Processing Block ID this data comes from."
    )

    spectral_window: SpectralWindow = Field(
        description="The spectral window from the data."
    )

    timestamp: str = Field(
        description="The timestamp this was generated at.",
        default_factory=lambda: datetime.now().astimezone().isoformat(),
    )

    data: list[
        (
            DataPayload
            | DataAndComponentPayload
            | SpectrumPayload
            | UVCoveragePayload
        )
    ] = Field(
        description="The list of data for this metric.",
        default_factory=lambda: [],
    )


def metrics_from_dict(data: dict) -> MetricPayload:
    """Convert a Metrics Dict back to a Metric Object."""
    return TypeAdapter(MetricPayload).validate_python(data)


@dataclass
class VisReceiveStatistics:
    """The Vis Receive payload statistics."""

    time: float = Field(
        description="The time that the payload was created at."
    )

    type: str = Field(
        default="visibility_receive",
        description="The type name of the payload. "
        'Will always be `"visibility_receive"`.',
    )

    state: str = Field(description="The current state of the receive process.")

    processing_block_id: str = Field(
        description="The current processing block ID."
    )

    execution_block_id: str = Field(
        description="The current execution block ID."
    )

    subarray_id: str = Field(description="The current subarray ID.")

    scan_id: int = Field(description="The scan ID.")

    payloads_received: int = Field(
        description="The amount of calls made to process function for "
        "this scan."
    )

    time_slices: int = Field(
        description="The amount of unique time slices we have received for "
        "this scan."
    )

    time_since_last_payload: float = Field(
        description="The amount of seconds since the last payload receved."
    )


@dataclass
class Spead2Stream:
    """The stats per stream in the Spead2 Receiver."""

    id: int = Field(description="The ID of the current stream.")

    heaps: int = Field(description="The number of heaps received.")

    blocked: int = Field(description="The number of blocked heaps.")

    incomplete_heaps: int = Field(
        description="The number of incomplete heaps."
    )


@dataclass
class Spead2Stats:
    """These are the stats that are sent by the Spead2 Receiver."""

    type: str = Field(
        default="receive_stats",
        description='The type of packet (always `"receive_stats"`)',
    )

    time: float = Field(description="The time that the packet was generated.")

    scan_id: int = Field(description="The scan ID.")

    state: str = Field(description="State of the current stream.")

    total_megabytes: float = Field(
        description="Total megabytes received for this receiver"
    )

    num_heaps: int = Field(
        description="The number of heaps that have been received."
    )

    num_incomplete: int = Field(
        description="The numbner of incomplete heaps that have been received."
    )

    duration: float = Field(description="The duration of the current receive.")

    streams: list[Spead2Stream] = Field(
        description="The list of current streams."
    )


def stats_from_dict(data: dict) -> VisReceiveStatistics | Spead2Stats:
    """Decode a stats packet based on the type parameter."""
    if data["type"] == "visibility_receive":
        return TypeAdapter(VisReceiveStatistics).validate_python(data)

    return TypeAdapter(Spead2Stats).validate_python(data)
