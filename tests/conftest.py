"""
Pytest Fixtures
"""

from dataclasses import asdict
from datetime import datetime
from unittest.mock import patch

import numpy
import pytest
from astropy import units
from astropy.coordinates import EarthLocation, SkyCoord
from ska_sdp_datamodels.calibration import PointingTable
from ska_sdp_datamodels.configuration import Configuration
from ska_sdp_datamodels.science_data_model import ReceptorFrame

from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    DataAndComponentPayload,
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
)


@pytest.fixture(name="phase_centre")
def phase_centre_fixture():
    """
    PhaseCentre fixture for Pointing Table schema
    """
    return SkyCoord(
        ra=+180.0 * units.deg,
        dec=-35.0 * units.deg,
        frame="icrs",
        equinox="J2000",
    )


@pytest.fixture(name="tel_config")
def config_fixture():
    """
    Configuration object fixture for Pointing Table schema
    It contains minimal information currently
    Just to pass the schema check
    """
    minimal_data = {
        "name": "mid-aa05",
        "names": numpy.array(["SKA001"]),
        "location": EarthLocation(
            x=units.Quantity(1, "m"),
            y=units.Quantity(1, "m"),
            z=units.Quantity(1, "m"),
        ),
    }
    return Configuration.constructor(**minimal_data)


@pytest.fixture(name="pointing_dataset")
def pointing_dataset_fxt(phase_centre, tel_config):
    """
    Provide Xarray Dataset to test the pointing table schema
    """

    pointing_ds = PointingTable.constructor(
        pointing=numpy.array([[[[[1.0, 1.0]]]]]),
        time=numpy.ones(1),
        interval=numpy.ones(1),
        weight=numpy.array([[[[[1.0, 1.0]]]]]),
        residual=numpy.array([[[[0.0, 0.0]]]]),
        frequency=numpy.ones(1),
        expected_width=numpy.array([[[[[1.0, 1.0]]]]]),
        fitted_width=numpy.array([[[[[1.0, 1.0]]]]]),
        fitted_width_std=numpy.array([[[[[0.0, 0.0]]]]]),
        fitted_height=numpy.array([[[[1.0]]]]),
        fitted_height_std=numpy.array([[[[0.0]]]]),
        receptor_frame=ReceptorFrame("stokesI"),
        pointing_frame="xel-el",
        band_type="Band 2",
        scan_mode="5-point",
        track_duration=20.0,
        discrete_offset=numpy.zeros(5 * 2),
        commanded_pointing=numpy.ones(2),
        pointingcentre=phase_centre,
        configuration=tel_config,
    )

    return pointing_ds


@pytest.fixture(name="pointing_numpy")
def pointing_numpy_fxt():
    """
    Provide data for testing the
    structured numpy array schema
    """
    data = {
        "antenna_name": "MKT001",
        "last_scan_index": 9.0,
        "xel_offset": 3.5,
        "xel_offset_std": 0.1,
        "el_offset": 2.8,
        "el_offset_std": 0.1,
        "expected_width_h": 1.1,
        "expected_width_v": 1.1,
        "fitted_width_h": 1.3,
        "fitted_width_h_std": 0.1,
        "fitted_width_v": 1.2,
        "fitted_width_v_std": 0.1,
        "fitted_height": 100.0,
        "fitted_height_std": 1.2,
    }

    return data


@pytest.fixture(name="signal_metrics")
@patch("ska_sdp_dataqueues.schemas.signal_display_metrics.datetime")
def signal_metrics_fxt(mock_datetime: datetime):
    """
    Provide data to test the Signal Display Metrics schema
    """
    mock_datetime.now.return_value = datetime(2024, 5, 2, 9, 42, 32)

    spectral_window = {
        "freq_min": 0.1,
        "freq_max": 0.2,
        "count": 1,
        "channels_id": 1,
        "spectral_window_id": 1,
        "start": 1,
        "stride": 1,
    }

    payload = MetricPayload(
        data_type=MetricDataTypes.PHASE,
        processing_block_id="pb-123",
        spectral_window=SpectralWindow(**spectral_window),
    )

    for x in range(3):
        payload.data.append(
            DataAndComponentPayload(
                baseline=f"C{x}_C{x*2}",
                polarisation="XX",
                data=[float(x), float(x) ** 2],
                component=[float(x), float(x) ** 2],
            )
        )

    data_dict = asdict(payload)
    data_dict["timestamp"] = data_dict["timestamp"][:19]

    return data_dict
