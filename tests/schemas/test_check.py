"""
Tests for schema check methods.
"""

from dataclasses import asdict
from typing import Union

import pytest

from ska_sdp_dataqueues.schemas.check import (
    check_data_list,
    check_other_schema,
    get_types,
)
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    DataPayload,
    SpectrumPayload,
)


def test_data_types():
    """Test case for schema data types"""

    input_values = [
        Union[str, int],
        list,
        float,
        list[float],
        list[Union[str, int]],
    ]

    expected = [[str, int], [list], [float], [float], [str, int]]

    for idx, value in enumerate(input_values):
        typs = get_types(value)
        assert typs == expected[idx]


def test_check_data_list():
    """Test case for schema list checks"""

    input_data = [
        asdict(DataPayload("C1_C2", "XX", [1.0, 2.0])),
        asdict(DataPayload("C2_C3", "XX", [3.0, 4.0])),
    ]
    schema_item = list[DataPayload]

    check_data_list(
        input_data,
        schema_item,
        "key",
    )


@pytest.mark.parametrize(
    "input_data, schema_item, expected_result",
    [
        (
            [
                asdict(
                    DataPayload(
                        baseline="C1_C2", polarisation="XX", data=[1.0, 2.0]
                    )
                )
            ],
            list[SpectrumPayload],
            "key: List item types must be in [<class 'ska_sdp_dataqueues."
            "schemas.signal_display_metrics.SpectrumPayload'>]",
        ),
        (
            [
                asdict(
                    DataPayload(
                        baseline="C1_C2", polarisation="XX", data=[1.0, 2.0]
                    )
                )
            ],
            list[float],
            "key: List item types must be in [<class 'float'>]",
        ),
        (
            [{"data": [1, 2, 3]}],
            DataPayload,
            "key: List item types must be in [<class 'ska_sdp_dataqueues."
            "schemas.signal_display_metrics.DataPayload'>]",
        ),
    ],
)
def test_check_data_list_bad_input(
    input_data: dict, schema_item: list, expected_result: str
):
    """Test case for schema list checks with bad input"""
    with pytest.raises(TypeError) as exception_info:
        check_data_list(input_data, schema_item, "key")
    assert str(exception_info.value) == expected_result


def test_check_other_schema(signal_metrics: dict):
    """Test case for other schema check with wrong schema class"""

    other_schema = {"antenna_name": "str", "last_scan_index": "float"}

    with pytest.raises(TypeError) as exception_info:
        check_other_schema(signal_metrics, other_schema)
    assert (
        str(exception_info.value)
        == "Schema must be a class with annotations defined as a dictionary"
    )
