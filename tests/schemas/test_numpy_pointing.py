# pylint: disable=no-member,unsubscriptable-object
# no-member: pylint doesn't recognize dataclass attributes

"""
Tests for structured numpy array for pointing.
"""

import numpy

from src.ska_sdp_dataqueues.schemas.numpy_structured_pointing import (
    PointingNumpyArray,
)


def test_numpy_structure(pointing_numpy):
    """
    PointingNumpyArray class correctly stores data
    """
    result = PointingNumpyArray(**pointing_numpy)

    for key, value in pointing_numpy.items():
        assert getattr(result, key) == value
    assert result.__annotations__.keys() == pointing_numpy.keys()

    # just check one value metadata, the rest work the same way
    assert (
        result.__dataclass_fields__["xel_offset"].metadata["unit"] == "degree"
    )


def test_numpy_structure_to_numpy(pointing_numpy):
    """
    Convert PointingNumpyArray to structured numpy array and
    concatenate multiple arrays.
    """
    numpy_data = PointingNumpyArray(**pointing_numpy)
    numpy_array = numpy_data.to_numpy()
    assert isinstance(numpy_array, numpy.ndarray)
    assert numpy_array.shape == (1,)
    assert len(numpy_array[0]) == 14

    # check that the types are converted correctly
    for key, value in numpy_data.__annotations__.items():
        assert numpy_array.dtype[key] == value
