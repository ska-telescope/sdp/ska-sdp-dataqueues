"""
Tests for the PointingTable schema
"""

import numpy
import pytest

pytest.importorskip("xradio")

from xradio.schema.check import check_dataset
from xradio.schema.dataclass import xarray_dataclass_to_dataset_schema

from ska_sdp_dataqueues.schemas.pointing_table import PointingTable


def test_check_pointing_fxt(pointing_dataset):
    """
    Test specific attributes of the pointing class
    Have been initialised properly
    """
    assert pointing_dataset.attrs["band_type"] == "Band 2"
    numpy.testing.assert_equal(
        pointing_dataset["pointing"], numpy.array([[[[[1, 1]]]]])
    )
    numpy.testing.assert_equal(
        pointing_dataset.attrs["configuration"]["names"],
        numpy.array(["SKA001"]),
    )


@pytest.mark.skip(
    reason="Schema validation doesnt work with real pointing table"
)
def test_check_dataset(pointing_dataset):
    """
    Check the Pointing dataset matches the xarray schema
    """
    pointing_schema = xarray_dataclass_to_dataset_schema(PointingTable)
    issues = check_dataset(pointing_dataset, pointing_schema)
    assert not issues
