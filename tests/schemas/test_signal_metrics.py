"""Tests to validate and show usage of the Signal Display Metrics
dataclasses."""

from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    DataAndComponentPayload,
    DataPayload,
    MetricDataTypes,
    MetricPayload,
    Spead2Stats,
    Spead2Stream,
    SpectralWindow,
    SpectrumPayload,
    UVCoveragePayload,
    VisReceiveStatistics,
    metrics_from_dict,
    stats_from_dict,
)


def test_normal(signal_metrics):
    """Example usage of the objects."""

    assert signal_metrics == {
        "data": [
            {
                "baseline": "C0_C0",
                "data": [
                    0.0,
                    0.0,
                ],
                "polarisation": "XX",
                "component": [0.0, 0.0],
            },
            {
                "baseline": "C1_C2",
                "data": [
                    1.0,
                    1.0,
                ],
                "polarisation": "XX",
                "component": [1.0, 1.0],
            },
            {
                "baseline": "C2_C4",
                "data": [
                    2.0,
                    4.0,
                ],
                "polarisation": "XX",
                "component": [2.0, 4.0],
            },
        ],
        "data_type": "phase",
        "processing_block_id": "pb-123",
        "spectral_window": {
            "count": 1,
            "freq_max": 0.2,
            "freq_min": 0.1,
            "channels_id": 1,
            "spectral_window_id": 1,
            "start": 1,
            "stride": 1,
        },
        "timestamp": "2024-05-02T09:42:32",
    }


def test_default_data_payload():
    """Using the default input layout and objects."""
    input_data = {
        "data_type": "bandaveragedxcorr",
        "processing_block_id": "pb-test-123",
        "spectral_window": {
            "freq_min": 1.0,
            "freq_max": 10.0,
            "count": 5,
            "channels_id": 100,
            "spectral_window_id": 1,
            "start": 1,
            "stride": 2,
        },
        "timestamp": "2024-01-01T12:12:12",
        "data": [
            {
                "baseline": "C0_C0",
                "data": [
                    0.0,
                    0.0,
                ],
                "polarisation": "XX",
            }
        ],
    }

    assert metrics_from_dict(input_data) == MetricPayload(
        data_type=MetricDataTypes.BAND_AVERAGED_X_CORR,
        processing_block_id="pb-test-123",
        spectral_window=SpectralWindow(
            freq_min=1.0,
            freq_max=10.0,
            count=5,
            channels_id=100,
            spectral_window_id=1,
            start=1,
            stride=2,
        ),
        timestamp="2024-01-01T12:12:12",
        data=[
            DataPayload(
                baseline="C0_C0",
                polarisation="XX",
                data=[
                    0.0,
                    0.0,
                ],
            )
        ],
    )


def test_data_and_component_payload():
    """Using the default input layout and objects."""
    input_data = {
        "data_type": "amplitude",
        "processing_block_id": "pb-test-123",
        "spectral_window": {
            "freq_min": 1.0,
            "freq_max": 10.0,
            "count": 5,
            "channels_id": 100,
            "spectral_window_id": 1,
            "start": 1,
            "stride": 2,
        },
        "timestamp": "2024-01-01T12:12:12",
        "data": [
            {
                "baseline": "C0_C0",
                "data": [
                    0.0,
                    0.0,
                ],
                "component": [
                    0.0,
                    0.0,
                ],
                "polarisation": "XX",
            }
        ],
    }

    assert metrics_from_dict(input_data) == MetricPayload(
        data_type=MetricDataTypes.AMPLITUDE,
        processing_block_id="pb-test-123",
        spectral_window=SpectralWindow(
            freq_min=1.0,
            freq_max=10.0,
            count=5,
            channels_id=100,
            spectral_window_id=1,
            start=1,
            stride=2,
        ),
        timestamp="2024-01-01T12:12:12",
        data=[
            DataAndComponentPayload(
                baseline="C0_C0",
                polarisation="XX",
                data=[
                    0.0,
                    0.0,
                ],
                component=[
                    0.0,
                    0.0,
                ],
            )
        ],
    )


def test_spectrum_payload():
    """Using the spectrum input layout and objects."""
    input_data = {
        "data_type": "spectrum",
        "processing_block_id": "pb-test-123",
        "spectral_window": {
            "freq_min": 1.0,
            "freq_max": 10.0,
            "count": 5,
            "channels_id": 100,
            "spectral_window_id": 1,
            "start": 1,
            "stride": 2,
        },
        "timestamp": "2024-01-01T12:12:12",
        "data": [
            {
                "polarisation": "XX",
                "power": [1, 2, 3],
                "angle": [1, 2, 3],
            }
        ],
    }

    assert metrics_from_dict(input_data) == MetricPayload(
        data_type=MetricDataTypes.SPECTRUM,
        processing_block_id="pb-test-123",
        spectral_window=SpectralWindow(
            freq_min=1.0,
            freq_max=10.0,
            count=5,
            channels_id=100,
            spectral_window_id=1,
            start=1,
            stride=2,
        ),
        timestamp="2024-01-01T12:12:12",
        data=[
            SpectrumPayload(
                polarisation="XX",
                power=[1, 2, 3],
                angle=[1, 2, 3],
            )
        ],
    )


def test_uv_coverage_payload():
    """Using the UV Coverage input layout and objects."""
    input_data = {
        "data_type": "uvcoverage",
        "processing_block_id": "pb-test-123",
        "spectral_window": {
            "freq_min": 1.0,
            "freq_max": 10.0,
            "count": 5,
            "channels_id": 100,
            "spectral_window_id": 1,
            "start": 1,
            "stride": 2,
        },
        "timestamp": "2024-01-01T12:12:12",
        "data": [
            {
                "baseline": "C0_C0",
                "polarisation": "XX",
                "weight": 3.0,
                "u": 1,
                "v": 2,
                "w": 3,
            }
        ],
    }

    assert metrics_from_dict(input_data) == MetricPayload(
        data_type=MetricDataTypes.UV_COVERAGE,
        processing_block_id="pb-test-123",
        spectral_window=SpectralWindow(
            freq_min=1.0,
            freq_max=10.0,
            count=5,
            channels_id=100,
            spectral_window_id=1,
            start=1,
            stride=2,
        ),
        timestamp="2024-01-01T12:12:12",
        data=[
            UVCoveragePayload(
                baseline="C0_C0",
                polarisation="XX",
                weight=3.0,
                u=1,
                v=2,
                w=3,
            )
        ],
    )


def test_spead2_empty():
    """Test the decode of an empty spead2 stats packet."""
    input_data = {
        "type": "receive_stats",
        "time": 1234567.1234,
        "scan_id": -1,
        "state": "no_stats",
        "total_megabytes": 0.0,
        "num_heaps": 0,
        "num_incomplete": 0,
        "duration": 0.0,
        "streams": [],
    }

    assert stats_from_dict(input_data) == Spead2Stats(
        type="receive_stats",
        time=1234567.1234,
        scan_id=-1,
        state="no_stats",
        total_megabytes=0.0,
        num_heaps=0,
        num_incomplete=0,
        duration=0.0,
        streams=[],
    )


def test_spead2_with_data():
    """Test the decode of a filled spead2 stats packet."""
    input_data = {
        "type": "receive_stats",
        "time": 1234567.1234,
        "scan_id": 1,
        "state": "stats",
        "total_megabytes": 1.32159,
        "num_heaps": 4,
        "num_incomplete": 0,
        "duration": 4.567,
        "streams": [
            {
                "id": 0,
                "heaps": 1,
                "blocked": 0,
                "incomplete_heaps": 0,
            },
            {
                "id": 1,
                "heaps": 1,
                "blocked": 0,
                "incomplete_heaps": 0,
            },
        ],
    }
    assert stats_from_dict(input_data) == Spead2Stats(
        type="receive_stats",
        time=1234567.1234,
        scan_id=1,
        state="stats",
        total_megabytes=1.32159,
        num_heaps=4,
        num_incomplete=0,
        duration=4.567,
        streams=[
            Spead2Stream(
                id=0,
                heaps=1,
                blocked=0,
                incomplete_heaps=0,
            ),
            Spead2Stream(
                id=1,
                heaps=1,
                blocked=0,
                incomplete_heaps=0,
            ),
        ],
    )


def test_visibility_receive_stats():
    """Test decode of the visibility receive stats packet."""
    input_data = {
        "time": 1234567.1234,
        "type": "visibility_receive",
        "state": "receiving",
        "processing_block_id": "pb-test-1234-1234",
        "execution_block_id": "eb-test-1234-1234",
        "subarray_id": "01",
        "scan_id": 1,
        "payloads_received": 5,
        "time_slices": 25,
        "time_since_last_payload": 3.4,
    }

    assert stats_from_dict(input_data) == VisReceiveStatistics(
        time=1234567.1234,
        type="visibility_receive",
        state="receiving",
        processing_block_id="pb-test-1234-1234",
        execution_block_id="eb-test-1234-1234",
        subarray_id="01",
        scan_id=1,
        payloads_received=5,
        time_slices=25,
        time_since_last_payload=3.4,
    )
