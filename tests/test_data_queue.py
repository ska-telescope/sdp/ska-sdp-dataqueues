"""Unit tests for data queues"""

import asyncio
import datetime
from unittest.mock import Mock

import numpy
import pytest
import pytest_asyncio
import xarray

from ska_sdp_dataqueues import (
    DataQueueAdmin,
    DataQueueConsumer,
    DataQueueProducer,
    Encoding,
)
from ska_sdp_dataqueues.data_queue import _decode, _encode
from ska_sdp_dataqueues.schemas.numpy_structured_pointing import (
    PointingNumpyArray,
)
from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricPayload

KAFKA_HOST = "localhost:9092"
TEST_TOPIC = "test-events"
TEST_DATA = numpy.array([1, 2, 3])


@pytest_asyncio.fixture(name="topic")
async def topic_fixture():
    """Data queue fixture"""

    # generate a unique topic name
    yield f"{TEST_TOPIC}-{datetime.datetime.now().microsecond}"


@pytest.mark.asyncio
async def test_data_queue_send_and_consume(topic: str):
    """
    Test case for send and receiving data to the queue as
    individual messages
    """
    messages = ["message1", "message2", TEST_DATA, {"key": "val"}]
    encodings = [
        Encoding.ASCII,
        Encoding.UTF8,
        Encoding.MSGPACK_NUMPY,
        Encoding.JSON,
    ]

    # user data_queue as a context manager to stop
    # consumer and producer at end of call

    for message, encoding in zip(messages, encodings):
        result = None
        async with DataQueueProducer(
            KAFKA_HOST, topic=topic, encoding=encoding
        ) as producer:
            await producer.send(message)

        async with DataQueueConsumer(
            KAFKA_HOST, topics=[topic], encoding=encoding
        ) as consumer:
            async for _, val in consumer:
                result = val
                break

        numpy.testing.assert_equal(message, result)


@pytest.mark.asyncio
async def test_data_queue_send_and_batch_consume(topic: str):
    """
    Test case for send and receiving data to the queue as a single
     array of messages
    """
    messages = ["message1", "message2", TEST_DATA]
    encodings = [
        Encoding.ASCII,
        Encoding.UTF8,
        Encoding.NPY,
        Encoding.MSGPACK_NUMPY,
    ]

    # Send all messages to Kafka
    for message, encoding in zip(messages, encodings):
        async with DataQueueProducer(KAFKA_HOST, topic=topic) as producer:
            await producer.send(message, encoding)

    received_messages = []
    async with DataQueueConsumer(KAFKA_HOST, topics=[topic]) as consumer:
        async for _, value in consumer:
            received_messages.append(value)
            if len(received_messages) == len(messages):
                break

    # Assert sent and decoded received messages match
    for sent, received, encoding in zip(
        messages, received_messages, encodings
    ):
        decoded = _decode(received, encoding)
        numpy.testing.assert_equal(sent, decoded)


@pytest.mark.skip(
    reason="Schema validation doesnt work with real pointing table"
)
@pytest.mark.asyncio
async def test_send_validate_pointing_table_as_datasetschema(
    pointing_dataset: xarray.Dataset, topic: str
):
    """
    Test case for send and validate PointingTable
    with correct data using xradio DatasetSchema as schema
    """
    pytest.importorskip("xradio")
    from xradio.schema.dataclass import xarray_dataclass_to_dataset_schema

    from ska_sdp_dataqueues.schemas.pointing_table import PointingTable

    encoding = "xarray"

    pointing_schema = xarray_dataclass_to_dataset_schema(PointingTable)

    # user data_queue as a context manager to stop
    # consumer and producer at end of call
    async with DataQueueProducer(KAFKA_HOST) as producer:
        # Send pointing table to Kafka with validation
        await producer.send(
            pointing_dataset,
            encoding=encoding,
            schema=pointing_schema,
            validate=True,
            topic=topic,
        )

    # Receive messages from Kafka
    received_message = None
    async with DataQueueConsumer(
        KAFKA_HOST, topics=[topic], encoding=encoding
    ) as consumer:
        async for _, message in consumer:
            received_message = message
            break

    # Assert sent and received messages match
    numpy.testing.assert_equal(received_message, pointing_dataset)


@pytest.mark.skip(
    reason="Schema validation doesnt work with real pointing table"
)
@pytest.mark.asyncio
async def test_send_validate_pointing_table_as_dataset(
    topic: str,
    pointing_dataset: xarray.Dataset,
):
    """
    Test case for send and validate PointingTable
    with correct data using xarray dataset as schema
    """
    pytest.importorskip("xradio")
    from ska_sdp_dataqueues.schemas.pointing_table import PointingTable

    encoding = "xarray"

    async with DataQueueProducer(KAFKA_HOST) as producer:
        # Send pointing table to Kafka with validation
        await producer.send(
            pointing_dataset,
            encoding=encoding,
            schema=PointingTable,
            validate=True,
            topic=topic,
        )

    # Receive messages from Kafka
    received_message = None
    async with DataQueueConsumer(
        KAFKA_HOST, topics=[topic], encoding=encoding
    ) as consumer:
        async for _, message in consumer:
            received_message = message
            break
    # Assert sent and received messages match
    numpy.testing.assert_equal(received_message, pointing_dataset)


@pytest.mark.asyncio
async def test_send_pointing_table_as_dataset_without_validation(
    topic: str,
    pointing_dataset: xarray.Dataset,
):
    """
    Test case for send PointingTable data without schema validation.
    """

    encoding = "xarray"
    async with DataQueueProducer(KAFKA_HOST) as producer:
        # Send pointing table to Kafka with validation
        await producer.send(
            pointing_dataset,
            encoding=encoding,
            schema=Mock(),
            validate=False,
            topic=topic,
        )

    # Receive messages from Kafka
    received_message = None
    async with DataQueueConsumer(
        KAFKA_HOST, topics=[topic], encoding=encoding
    ) as consumer:
        async for _, message in consumer:
            received_message = message
            break

    # Assert sent and received messages match
    numpy.testing.assert_equal(received_message, pointing_dataset)


@pytest.mark.asyncio
async def test_send_validate_incorrect_pointing_table(
    topic: str,
    pointing_dataset: xarray.Dataset,
):
    """
    Test case for send and validate PointingTable
    with incorrect data according to schema
    """
    pytest.importorskip("xradio")
    from xradio.schema.dataclass import xarray_dataclass_to_dataset_schema

    from ska_sdp_dataqueues.schemas.pointing_table import PointingTable

    encoding = "xarray"

    # Change pointing dataset so it does not match schema
    pointing_dataset.attrs["pointing_frame"] = 1.0

    pointing_schema = xarray_dataclass_to_dataset_schema(PointingTable)

    with pytest.raises(TypeError) as exception_info:
        async with DataQueueProducer(KAFKA_HOST) as producer:
            # Send pointing table to Kafka with validation
            await producer.send(
                pointing_dataset,
                encoding=encoding,
                schema=pointing_schema,
                validate=True,
                topic=topic,
            )
    assert (
        "\n * Schema issue with attrs['pointing_frame']: float is not "
        "an instance of str (expected: <class 'str'> found: <class 'float'>)"
        in str(exception_info.value)
    )


@pytest.mark.asyncio
async def test_send_validate_incorrect_schema_class(
    topic: str,
    pointing_numpy: dict,
):
    """
    Test case for send and validate pointing numpy array
    with incorrect schema class
    """

    # Use dictionary instead of dataclass
    other_schema = {"antenna_name": "str", "last_scan_index": "float"}

    with pytest.raises(TypeError) as exception_info:
        async with DataQueueProducer(KAFKA_HOST) as producer:
            # Send pointing table to Kafka with validation
            await producer.send(
                pointing_numpy,
                encoding=Encoding.UTF8,
                schema=other_schema,
                validate=True,
                topic=topic,
            )
    assert (
        str(exception_info.value)
        == "Schema must be xradio DatasetSchema or a dataclass"
    )


@pytest.mark.asyncio
async def test_send_validate_incorrect_input_data(topic: str):
    """
    Test case for send and validate with incorrect input data type
    """

    # Use simple dataclass instead of converting to xarray schema
    pointing_dataset = [1, 2, 3, 4, 5]

    with pytest.raises(TypeError) as exception_info:
        async with DataQueueProducer(KAFKA_HOST) as producer:
            # Send pointing table to Kafka with validation
            await producer.send(
                pointing_dataset,
                encoding=Encoding.UTF8,
                schema=PointingNumpyArray,
                validate=True,
                topic=topic,
            )
    assert (
        str(exception_info.value)
        == "<class 'list'> cannot currently be validated - "
        "must be numpy array or dictionary"
    )


@pytest.mark.asyncio
async def test_data_queue_send_schema_validate_numpy(
    topic: str, pointing_numpy: dict
):
    """
    Test case for send and validate Numpy structured array
    """

    pointing_data_numpy = PointingNumpyArray(**pointing_numpy).to_numpy()

    async with DataQueueProducer(KAFKA_HOST) as producer:
        # Send pointing table to Kafka with validation
        await producer.send(
            pointing_data_numpy,
            encoding=Encoding.MSGPACK_NUMPY,
            schema=PointingNumpyArray,
            validate=True,
            topic=topic,
        )

    # Receive messages from Kafka
    received_message = None
    async with DataQueueConsumer(
        KAFKA_HOST, topics=[topic], encoding=Encoding.MSGPACK_NUMPY
    ) as consumer:
        async for _, message in consumer:
            received_message = message
            break

    # Assert sent and received messages match
    numpy.testing.assert_equal(received_message, pointing_data_numpy)


@pytest.mark.asyncio
async def test_send_schema_validate_incorrect_numpy(topic: str):
    """
    Test case for send and validate Numpy structured array
    with incorrect input format
    """

    incorrect_pointing = numpy.array(
        [(1.0, 9.0, 3.5)],
        dtype=[
            ("antenna_name", "<f8"),
            ("last_scan_index", "<f8"),
            ("xel_offset", "<f8"),
        ],
    )

    with pytest.raises(TypeError) as exception_info:
        async with DataQueueProducer(KAFKA_HOST) as producer:
            # Send pointing table to Kafka with validation
            await producer.send(
                incorrect_pointing,
                encoding=Encoding.MSGPACK_NUMPY,
                schema=PointingNumpyArray,
                validate=True,
                topic=topic,
            )
    assert str(exception_info.value) == "antenna_name: float64 != <U8"


@pytest.mark.asyncio
async def test_data_queue_send_schema_validate_metrics(
    topic: str, signal_metrics
):
    """Test case for send and validate Signal Display Metrics"""

    encoding = Encoding.MSGPACK_NUMPY

    async with DataQueueProducer(KAFKA_HOST) as producer:
        # Send pointing table to Kafka with validation
        await producer.send(
            signal_metrics,
            encoding=encoding,
            schema=MetricPayload,
            validate=True,
            topic=topic,
        )

    # Receive messages from Kafka
    received_message = None
    async with DataQueueConsumer(
        KAFKA_HOST, topics=[topic], encoding=encoding
    ) as consumer:
        async for _, message in consumer:
            received_message = message
            break

    # Assert sent and received messages match
    assert numpy.array_equal(signal_metrics, received_message)


@pytest.mark.asyncio
async def test_data_schema_validate_incorrect_metrics_spectral_window(
    topic: str, signal_metrics: dict
):
    """
    Test case for incorrect Signal Display Metrics attribute in
    Spectral Window object.
    """

    # Change from float to integer
    signal_metrics["spectral_window"]["freq_min"] = 1300

    # Send dictionary to Kafka with validation
    with pytest.raises(TypeError) as exception_info:
        async with DataQueueProducer(KAFKA_HOST) as producer:
            # Send pointing table to Kafka with validation
            await producer.send(
                signal_metrics,
                encoding=Encoding.MSGPACK_NUMPY,
                schema=MetricPayload,
                validate=True,
                topic=topic,
            )
    assert (
        str(exception_info.value)
        == "[spectral_window][freq_min]: <class 'int'> != <class 'float'>"
    )


@pytest.mark.asyncio
async def test_data_schema_validate_incorrect_metrics_pbid(
    topic: str, signal_metrics: dict
):
    """
    Test case for incorrect Signal Display Metrics
    attribute
    """

    # Change from float to integer
    signal_metrics["processing_block_id"] = 1.35

    # Send dictionary to Kafka with validation
    with pytest.raises(TypeError) as exception_info:
        async with DataQueueProducer(KAFKA_HOST) as producer:
            # Send pointing table to Kafka with validation
            await producer.send(
                signal_metrics,
                encoding=Encoding.MSGPACK_NUMPY,
                schema=MetricPayload,
                validate=True,
                topic=topic,
            )
    assert (
        str(exception_info.value)
        == "processing_block_id: <class 'float'> != <class 'str'>"
    )


@pytest.mark.parametrize(
    "value, encoding, expected_result",
    [
        (
            TEST_DATA,
            Encoding.MSGPACK_NUMPY,
            b"\x85\xc4\x02nd\xc3\xc4\x04type\xa3<i8\xc4\x04"
            b"kind\xc4\x00\xc4\x05shape\x91\x03\xc4\x04data\xc4"
            b"\x18\x01\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00"
            b"\x00\x00\x00\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00",
        ),
        (
            TEST_DATA,
            Encoding.NPY,
            b"\x93NUMPY\x01\x00v\x00"
            b"{'descr': '<i8', 'fortran_order': False, 'shape': (3,), }"
            b"                                                            "
            b"\n\x01\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00"
            b"\x00\x03\x00\x00\x00\x00\x00\x00\x00",
        ),
        (TEST_DATA.tolist(), Encoding.JSON, b"[1, 2, 3]"),
        (TEST_DATA, Encoding.UTF8, b"[1 2 3]"),
        (TEST_DATA, Encoding.ASCII, b"[1 2 3]"),
    ],
)
def test_encode(value: numpy.ndarray, encoding: str, expected_result: bytes):
    """
    Test case for handling different encoding options
    for encode method
    """
    result = _encode(value, encoding)
    numpy.testing.assert_equal(result, expected_result)


@pytest.mark.parametrize(
    "value_bytes, encoding, expected_result",
    [
        (
            b"\x85\xc4\x02nd\xc3\xc4\x04type\xa3<i8\xc4\x04"
            b"kind\xc4\x00\xc4\x05shape\x91\x03\xc4\x04data\xc4"
            b"\x18\x01\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00"
            b"\x00\x00\x00\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00",
            Encoding.MSGPACK_NUMPY,
            TEST_DATA,
        ),
        (
            b"\x93NUMPY\x01\x00v\x00"
            b"{'descr': '<i8', 'fortran_order': False, 'shape': (3,), }"
            b"                                                            "
            b"\n\x01\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00"
            b"\x00\x03\x00\x00\x00\x00\x00\x00\x00",
            Encoding.NPY,
            TEST_DATA,
        ),
        (b"[1, 2, 3]", Encoding.JSON, [1, 2, 3]),
        (b"[1 2 3]", Encoding.UTF8, "[1 2 3]"),
        (b"[1 2 3]", Encoding.ASCII, "[1 2 3]"),
    ],
)
def test_decode(
    value_bytes: bytes, encoding: str, expected_result: numpy.ndarray
):
    """
    Test case for handling different encoding options
    for decode method
    """
    result = _decode(value_bytes, encoding)
    numpy.testing.assert_equal(result, expected_result)


def test_encode_error_handling():
    """Test case for handling error when encoding is not supported"""
    unsupported_encoding = "unsupported_encoding"
    with pytest.raises(ValueError):
        _encode(TEST_DATA, unsupported_encoding)


def test_decode_error_handling():
    """Test case for handling error when encoding is not supported"""
    unsupported_encoding = "unsupported_encoding"
    with pytest.raises(ValueError):
        _decode(b"[1 2 3]", unsupported_encoding)


def test_encode_decode_pointingtable(
    pointing_dataset: xarray.Dataset,
):
    """
    Test case for handling encoding and decoding of xarray pointing table
    """
    encoding = "xarray"
    encoded_dataset = _encode(pointing_dataset, encoding)
    decoded_dataset = _decode(encoded_dataset, encoding)
    numpy.testing.assert_equal(pointing_dataset, decoded_dataset)


@pytest.mark.asyncio
async def test_send_encoded_receive_bytes(topic: str):
    """Send encoded data, but receive bytes"""

    message_count = 5

    async with DataQueueProducer(
        KAFKA_HOST, topic=topic, encoding=Encoding.JSON
    ) as producer:
        for _ in range(message_count):
            await producer.send({"key": "value"})

    messages = []
    async with DataQueueConsumer(KAFKA_HOST, topics=[topic]) as consumer:
        async for topic, message in consumer:
            messages.append((topic, message))
            if len(messages) == message_count:
                break

    expected_data = [
        (topic, b'{"key": "value"}') for i in range(message_count)
    ]

    assert messages == expected_data


@pytest.mark.asyncio
async def test_multi_topics_change_topic_after_reading(topic: str):
    """Test for starting consumer, sending,
    then setting the topic to listen to. (Use case for NALEDI)"""

    message_count = 5
    messages = []

    async def receive(consumer: DataQueueConsumer):
        async for topic, message in consumer:
            messages.append((topic, message))
            if len(messages) == message_count:
                return

    async with DataQueueProducer(
        KAFKA_HOST, topic=topic, encoding=Encoding.JSON
    ) as producer:
        async with DataQueueConsumer(
            KAFKA_HOST, encoding=Encoding.JSON
        ) as consumer:
            gathered_task = asyncio.gather(receive(consumer))

            for i in range(message_count):
                await producer.send({"data": "sample", "record": i})

            await asyncio.sleep(
                0.2
            )  # give enough time for receive task to start

            await consumer.update_topics(topics=[topic])
            await gathered_task

    expected_data = [
        (topic, {"data": "sample", "record": i}) for i in range(message_count)
    ]

    assert messages == expected_data


@pytest.mark.asyncio
async def test_multi_topics_change_topics_after_reading_topic_list(topic: str):
    """Test for starting consumer, sending (to multiple topics),
    then setting the topic to listen to, using a list of topics.
    (Use case for NALEDI)"""

    message_count = 5
    messages = []

    async def receive(consumer: DataQueueConsumer):
        async for topic, message in consumer:
            messages.append((topic, message))
            if len(messages) == message_count:
                return

    async with DataQueueProducer(
        KAFKA_HOST, encoding=Encoding.JSON
    ) as producer:
        async with DataQueueConsumer(
            KAFKA_HOST, encoding=Encoding.JSON
        ) as consumer:
            gathered_task = asyncio.gather(receive(consumer))
            for i in range(message_count):
                await producer.send(
                    {"data": "sample", "record": i},
                    topic=f"{topic}_{i}",
                )

            await asyncio.sleep(
                0.2
            )  # give enough time for receive task to start

            await consumer.update_topics(
                topics=[f"{topic}_{i}" for i in range(message_count)]
            )
            await gathered_task

    expected_data = [
        (f"{topic}_{i}", {"data": "sample", "record": i})
        for i in range(message_count)
    ]

    messages.sort()  # the messages are not always going to be in order

    assert messages == expected_data


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "topic_list",
    [
        (None),
        ([]),
    ],
)
async def test_multi_topics_subscribe(topic_list, topic: str):
    """Check unsubscribe is correctly called"""

    # pylint: disable=protected-access

    async with DataQueueConsumer(KAFKA_HOST) as consumer:

        await consumer.update_topics(topics=[topic])
        assert [str(s) for s in consumer._consumer.subscription()] == [topic]

        await consumer.update_topics(topics=topic_list)
        assert [str(s) for s in consumer._consumer.subscription()] == []

        await asyncio.sleep(0.1)  # avoids a cancelled error


@pytest.mark.asyncio
async def test_multi_consumer_same_topic(topic: str):
    """
    When two consumers are listening to the same topic,
    they both will only get all the data, if the consumer
    was started without group ids set.

    If group id is set then the second consumer will hang
    waiting for the message that it will not get.
    """

    expected_message = "test-message"
    encoding = Encoding.UTF8

    messages = {}
    async with DataQueueAdmin(server=KAFKA_HOST) as admin:
        await admin.create_topics([(topic, 1)])

    async def receive(name):
        async with DataQueueConsumer(
            KAFKA_HOST, encoding=encoding
        ) as consumer:
            await consumer.update_topics(topics=[topic])
            async for _, message in consumer:
                messages[name] = message
                break

    gathered_task1 = asyncio.gather(receive("dq1"))
    gathered_task2 = asyncio.gather(receive("dq2"))

    async with DataQueueProducer(
        KAFKA_HOST, topic=topic, encoding=encoding
    ) as producer:
        await producer.send(expected_message)

    await gathered_task1
    await gathered_task2

    assert messages["dq1"] == expected_message  # from data_queue1
    assert messages["dq2"] == expected_message  # from data_queue2


@pytest.mark.asyncio
async def test_send_multiple_partitons(topic: str):
    """Use a producer to send data to multiple partitions, with a single
    receiver getting from all of them."""
    partitions = 5
    messages_per_partition = 5

    messages = []

    async with DataQueueAdmin(server=KAFKA_HOST) as admin:
        await admin.create_topics([(topic, partitions)])

    async def _receive():
        async with DataQueueConsumer(
            KAFKA_HOST,
            topics=[topic],
            encoding=Encoding.UTF8,
            set_group_id=False,
            set_consumer_offsets=False,
        ) as consumer:
            await consumer.update_topics(topics=[topic])
            async for _, message in consumer:
                messages.append(message)
                if len(messages) == messages_per_partition * partitions:
                    return

    gathered_task = asyncio.gather(_receive())
    async with DataQueueProducer(
        KAFKA_HOST, topic=topic, encoding=Encoding.UTF8
    ) as producer:
        for message in range(messages_per_partition):
            for partition in range(partitions):
                await producer.send(
                    f"{message}-{partition}", partition=partition
                )

    try:
        await asyncio.wait_for(gathered_task, timeout=2)
    except asyncio.TimeoutError:
        assert False, "Test timed out before data was received"

    assert sorted(messages) == sorted(
        [
            f"{message}-{partition}"
            for partition in range(partitions)
            for message in range(messages_per_partition)
        ]
    )


@pytest.mark.asyncio
async def test_send_and_receive_multiple_partitions(topic: str):
    """Use a single producer to send data to multiple receivers, using multiple
    partitions."""
    partitions = 2
    messages_per_partition = 5

    messages = [[] for p in range(partitions)]

    async with DataQueueAdmin(server=KAFKA_HOST) as admin:
        await admin.create_topics([(topic, partitions)])

    async def _receive(partition: int):
        async with DataQueueConsumer(
            KAFKA_HOST,
            encoding=Encoding.UTF8,
            auto_offset_reset="earliest",
        ) as consumer:
            await consumer.assign_topics([(topic, partition)])
            async for _, message in consumer:
                messages[partition].append(message)
                if len(messages[partition]) == messages_per_partition:
                    return

    tasks = [asyncio.gather(_receive(p)) for p in range(partitions)]

    async with DataQueueProducer(
        KAFKA_HOST, topic=topic, encoding=Encoding.UTF8
    ) as producer:
        for message in range(messages_per_partition):
            for partition in range(partitions):
                await producer.send(
                    f"{message}-{partition}", partition=partition
                )

    current_index = 0
    try:
        for index, task in enumerate(tasks):
            current_index = index
            await asyncio.wait_for(task, timeout=5)
    except asyncio.TimeoutError:
        assert (
            False
        ), f"Test timed out before data was received: task-{current_index}"

    assert sorted(messages) == sorted(
        [
            [
                f"{message}-{partition}"
                for message in range(messages_per_partition)
            ]
            for partition in range(partitions)
        ]
    )
